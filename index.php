<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Aertrip - Career Opportunity - Senior Backend Developer - Atul N Thorat</title>
        <link rel="stylesheet" type="text/css"href="css/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="css/jquerysctipttop.css">        
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"  media="screen" >
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <div class="container">        
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <!-- Repeater Html Start -->               

                <div class="clearfix"></div>
                <div id="repeater" class="sortable">
                    <!-- Repeater Heading -->
                    <div class="repeater-heading">                        
                        <button class="btn btn-primary pt-5 pull-right repeater-add-btn">
                            Add
                        </button>
                    </div>
                    <div class="clearfix"></div>
                    <!-- Repeater Items -->
                    <div class="items ui-state-default" data-group="test">
                        <!-- Repeater Content -->
                        <div class="item-content">
                            <div class="form-group">
                                <img class="pull-left" src="images/sort.png" height="25" width="25" />
                                <div class="col-lg-10">
                                    <input type="url" class="form-control txtUrl" id="txtUrl0" placeholder="Url" data-name="url">
                                    <span class="errorUrl" style="display:none;"> Invalid Url </span>                                                                       
                                </div>
                            </div>
                        </div>
                        <!-- Repeater Remove Btn -->
                        <div class="pull-right repeater-remove-btn" style="margin-top:20px">
                            <button id="remove-btn" class="btn btn-danger" onclick="$(this).parents('.items').remove()">Remove</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div>
                    <input class="btn btn-primary" type="button" value="submit" id="submit" name="submit">
                </div>

                <div id="progressbar" style="display:none;">                
                    <div class="progress-label">Loading...</div>
                </div>
                <!-- Repeater End -->
            </div>
        </div>
    </div>
    <script src="js/jquery-1.12.4.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="js/jquery-repeater.js" type="text/javascript"></script>
    <script src="js/custom.js" type="text/javascript"></script>
</body>
</html>
