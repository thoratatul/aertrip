<?php

$url = $_POST['url'];
$page = file_get_contents($url);
if (!$page)
    echo $response['status'] = 'error';

$res = preg_match("/<title>(.*)<\/title>/siU", $page, $title_matches);
if (!$res)
    $response['status'] = 'error';
else {
// Clean up title: remove EOL's and excessive whitespace.
    $title = preg_replace('/\s+/', ' ', $title_matches[1]);
    $title = trim($title);
    $response['status'] = 'success';
    $response['title'] = $title;
}
echo json_encode($response);
